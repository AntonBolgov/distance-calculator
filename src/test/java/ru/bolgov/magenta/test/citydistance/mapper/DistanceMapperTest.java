package ru.bolgov.magenta.test.citydistance.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.bolgov.magenta.test.citydistance.exception.CityNotFoundException;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.DistanceDto;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;
import ru.bolgov.magenta.test.citydistance.model.entiny.Distance;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class DistanceMapperTest {

    @Test
    void dtoToEntity() {
        CityRepository cityRepository = Mockito.mock(CityRepository.class);
        DistanceMapper distanceMapper = new DistanceMapper(cityRepository);

        DistanceDto distanceDto = new DistanceDto();
        distanceDto.setCityFrom("from");
        distanceDto.setCityTo("to");
        distanceDto.setDistance(100);
        Throwable thrown = assertThrows(CityNotFoundException.class, () -> {
           distanceMapper.dtoToEntity(distanceDto);
        });

        Mockito.when(cityRepository.findByCityName(Mockito.anyString())).thenReturn(Optional.ofNullable(new City()));
        Distance distance = distanceMapper.dtoToEntity(distanceDto);
        Assertions.assertEquals(distanceDto.getDistance(), distance.getDistance());
    }
}