package ru.bolgov.magenta.test.citydistance.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.web.multipart.MultipartFile;
import ru.bolgov.magenta.test.citydistance.model.dto.city.CitiesResponse;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;
import ru.bolgov.magenta.test.citydistance.repository.DistanceRepository;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;

class CityServiceTest {

    @Test
    public void updateCityMatrixTest() throws IOException {
        DistanceRepository distanceRepository = Mockito.mock(DistanceRepository.class);
        CityRepository cityRepository = Mockito.mock(CityRepository.class);

        CityService cityService = new CityService(distanceRepository, cityRepository);
        MultipartFile multipartFile = Mockito.mock(MultipartFile.class);
        Mockito.when(cityRepository.findByCityName(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
        String cities = "<Cities>\n" +
                "\t<cities>\n" +
                "\t\t<cities>\n" +
                "\t\t\t<cityName>samara</cityName>\n" +
                "\t\t\t<latitude>53.202778</latitude>\n" +
                "\t\t\t<longitude>50.1140833</longitude>\n" +
                "\t\t</cities>\n" +
                "\t</cities>\n" +
                "</Cities>";
        Mockito.when(multipartFile.getBytes()).thenReturn(cities.getBytes(StandardCharsets.UTF_8));
        cityService.updateCityMatrix(multipartFile);
        Mockito.verify(cityRepository, Mockito.times(1)).findByCityName(Mockito.anyString());
        Mockito.verify(cityRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void getAllCitiesTest(){
        DistanceRepository distanceRepository = Mockito.mock(DistanceRepository.class);
        CityRepository cityRepository = Mockito.mock(CityRepository.class);

        City city = new City();
        city.setCityName("city");
        CityService cityService = new CityService(distanceRepository, cityRepository);
        Mockito.when(cityRepository.findAll()).thenReturn(Arrays.asList(city));
        CitiesResponse cities = cityService.getAllCities();
        Mockito.verify(cityRepository, Mockito.times(1)).findAll();
        String cityName = cities.getCities().get(0).getCityName();
        Assertions.assertEquals("city", cityName);
    }
}