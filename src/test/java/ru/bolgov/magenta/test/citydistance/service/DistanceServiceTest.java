package ru.bolgov.magenta.test.citydistance.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.web.multipart.MultipartFile;
import ru.bolgov.magenta.test.citydistance.exception.CityNotFoundException;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.CalculateResponse;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;
import ru.bolgov.magenta.test.citydistance.model.entiny.Distance;
import ru.bolgov.magenta.test.citydistance.myenum.QueryTypeEnum;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;
import ru.bolgov.magenta.test.citydistance.repository.DistanceRepository;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class DistanceServiceTest {

    @Test
    void updateDistanceMatrix() throws IOException {
        DistanceRepository distanceRepository = Mockito.mock(DistanceRepository.class);
        CityRepository cityRepository = Mockito.mock(CityRepository.class);
        DistanceService distanceService = new DistanceService(cityRepository, distanceRepository);
        MultipartFile file = Mockito.mock(MultipartFile.class);
        String distances = "<Distance>\n" +
                "<distanceList>\n" +
                "<distanceList>\n" +
                "\t<cityFrom>samara</cityFrom>\n" +
                "\t<cityTo>moscow</cityTo>\n" +
                "\t<distance>1000</distance>\n" +
                "</distanceList>\n" +
                "</distanceList>\n" +
                "</Distance>";
        Mockito.when(file.getBytes()).thenReturn(distances.getBytes(StandardCharsets.UTF_8));
        Throwable thrown = assertThrows(CityNotFoundException.class, () -> {
            distanceService.updateDistanceMatrix(file);
        });

        Mockito.when(cityRepository.findByCityName(Mockito.anyString())).thenReturn(Optional.of(new City()));
        distanceService.updateDistanceMatrix(file);
        Mockito.verify(distanceRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(cityRepository, Mockito.times(3)).findByCityName(Mockito.anyString());
    }

    @Test
    void parseQueryCrowflight() {
        DistanceRepository distanceRepository = Mockito.mock(DistanceRepository.class);
        CityRepository cityRepository = Mockito.mock(CityRepository.class);
        DistanceService distanceService = new DistanceService(cityRepository, distanceRepository);
        City samara = new City();
        samara.setCityName("samara");
        samara.setLatitude(53.202778d);
        samara.setLongitude(50.1140833d);
        City singapore = new City();
        singapore.setCityName("singapore");
        singapore.setLatitude(1.352083d);
        singapore.setLongitude(103.819836d);
        Mockito.when(cityRepository.findByCityName("samara")).thenReturn(Optional.of(samara));
        Mockito.when(cityRepository.findByCityName("singapore")).thenReturn(Optional.of(singapore));
        CalculateResponse calculateResponse = distanceService.parseQuery("CROWFLIGHT", "samara", "singapore");
        Assertions.assertEquals(7576, calculateResponse.getDistanceResponses().get(0).getDistances().getDistance(), 100);
    }

    @Test
    void parseQueryMatrix(){
        DistanceRepository distanceRepository = Mockito.mock(DistanceRepository.class);
        CityRepository cityRepository = Mockito.mock(CityRepository.class);
        DistanceService distanceService = new DistanceService(cityRepository, distanceRepository);

        Distance distance = new Distance();
        distance.setFromCityId(1l);
        distance.setToCityId(2l);
        distance.setDistance(100);
        Mockito.when(distanceRepository.getByCities(1l, 2l)).thenReturn(Optional.of(distance));

        City city1 = new City();
        city1.setCityName("1");
        city1.setCityId(1l);
        City city2 = new City();
        city2.setCityName("2");
        city2.setCityId(2l);
        Mockito.when(cityRepository.findByCityName("1")).thenReturn(Optional.of(city1));
        Mockito.when(cityRepository.findByCityName("2")).thenReturn(Optional.of(city2));

        CalculateResponse calculateResponse = distanceService.parseQuery("MATRIX", "1", "2");
        Assertions.assertEquals(distance.getDistance(), calculateResponse.getDistanceResponses().get(0).getDistances().getDistance());
    }

    @Test
    void parseQueryAll(){
        DistanceRepository distanceRepository = Mockito.mock(DistanceRepository.class);
        CityRepository cityRepository = Mockito.mock(CityRepository.class);
        DistanceService distanceService = new DistanceService(cityRepository, distanceRepository);

        Distance distance = new Distance();
        distance.setFromCityId(1l);
        distance.setToCityId(2l);
        distance.setDistance(100);
        Mockito.when(distanceRepository.getByCities(1l, 2l)).thenReturn(Optional.of(distance));

        City city1 = new City();
        city1.setCityName("1");
        city1.setCityId(1l);
        city1.setLatitude(53.202778d);
        city1.setLongitude(50.1140833d);
        City city2 = new City();
        city2.setCityName("2");
        city2.setCityId(2l);
        city2.setLatitude(1.352083d);
        city2.setLongitude(103.819836d);
        Mockito.when(cityRepository.findByCityName("1")).thenReturn(Optional.of(city1));
        Mockito.when(cityRepository.findByCityName("2")).thenReturn(Optional.of(city2));

        CalculateResponse calculateResponse = distanceService.parseQuery("ALL", "1", "2");

        int crowflightDistance = calculateResponse.getDistanceResponses().stream()
                .filter(x -> x.getCalculationType().equals(QueryTypeEnum.CROWFLIGHT.toString()))
                .findAny().get().getDistances().getDistance();
        int matrixDistance = calculateResponse.getDistanceResponses().stream()
                .filter(x -> x.getCalculationType().equals(QueryTypeEnum.MATRIX.toString()))
                .findAny().get().getDistances().getDistance();
        Assertions.assertEquals(7576, crowflightDistance, 100);
        Assertions.assertEquals(distance.getDistance(), matrixDistance);
    }
}