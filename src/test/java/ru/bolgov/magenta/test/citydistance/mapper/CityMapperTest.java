package ru.bolgov.magenta.test.citydistance.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.bolgov.magenta.test.citydistance.model.dto.city.CityResponse;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;

import static org.junit.jupiter.api.Assertions.*;

class CityMapperTest {

    @Test
    void cityToDto() {
        City city = new City();
        city.setCityName("city");
        CityMapper cityMapper = new CityMapper();
        CityResponse cityResponse = cityMapper.cityToDto(city);
        Assertions.assertEquals("city", cityResponse.getCityName());
    }
}