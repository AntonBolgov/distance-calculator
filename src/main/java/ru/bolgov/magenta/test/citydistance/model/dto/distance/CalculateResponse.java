package ru.bolgov.magenta.test.citydistance.model.dto.distance;

import java.util.List;

public class CalculateResponse {
    private List<DistanceResponse> distanceResponses;

    public List<DistanceResponse> getDistanceResponses() {
        return distanceResponses;
    }

    public void setDistanceResponses(List<DistanceResponse> distanceResponses) {
        this.distanceResponses = distanceResponses;
    }
}
