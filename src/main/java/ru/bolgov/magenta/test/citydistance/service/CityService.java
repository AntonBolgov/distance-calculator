package ru.bolgov.magenta.test.citydistance.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.bolgov.magenta.test.citydistance.mapper.CityMapper;
import ru.bolgov.magenta.test.citydistance.model.dto.city.Cities;
import ru.bolgov.magenta.test.citydistance.model.dto.city.CitiesResponse;
import ru.bolgov.magenta.test.citydistance.model.dto.city.CityResponse;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;
import ru.bolgov.magenta.test.citydistance.repository.DistanceRepository;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CityService {

    private DistanceRepository distanceRepository;

    private CityRepository cityRepository;

    public CityService(DistanceRepository distanceRepository, CityRepository cityRepository){
        this.cityRepository = cityRepository;
        this.distanceRepository = distanceRepository;
    }

    public void updateCityMatrix(MultipartFile file) throws IOException {
        XmlMapper mapper = new XmlMapper();
        Charset utfCharset = Charset.forName("UTF-8");
        String input = new String(file.getBytes(), utfCharset);
        Cities cities = mapper.readValue(input, Cities.class);

        cities.getCities().stream()
                .filter(city -> Objects.isNull(cityRepository
                        .findByCityName(city.getCityName().toLowerCase(Locale.ROOT))
                        .orElse(null)))
                .map(city ->{
                    cityRepository.save(city);
                    return false;
                })
                .count();

    }

    public CitiesResponse getAllCities(){
        CitiesResponse citiesResponse = new CitiesResponse();
        CityMapper cityMapper = new CityMapper();
        List<City> cities = cityRepository.findAll();
        List<CityResponse> cityResponses= cities.stream()
                .map(city -> {
                    return cityMapper.cityToDto(city);
                }).collect(Collectors.toList());
        citiesResponse.setCities(cityResponses);
        return citiesResponse;
    }
}
