package ru.bolgov.magenta.test.citydistance.mapper;

import ru.bolgov.magenta.test.citydistance.model.dto.city.CityResponse;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;

public class CityMapper {

    public CityResponse cityToDto(City city){
        CityResponse cityResponse = new CityResponse();
        cityResponse.setId(city.getCityId());
        cityResponse.setCityName(city.getCityName());
        return cityResponse;
    }
}
