package ru.bolgov.magenta.test.citydistance.model.dto.city;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;

import java.util.List;

@JacksonXmlRootElement(localName = "Cities")
public class Cities {

    private List<City> cities;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
