package ru.bolgov.magenta.test.citydistance.mapper;

import org.springframework.stereotype.Service;
import ru.bolgov.magenta.test.citydistance.exception.CityNotFoundException;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.DistanceDto;
import ru.bolgov.magenta.test.citydistance.model.entiny.Distance;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;

@Service
public class DistanceMapper {

    private final CityRepository cityRepository;

    public DistanceMapper(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public Distance dtoToEntity(DistanceDto distanceDto) throws CityNotFoundException{
        Distance distance = new Distance();
        try {
            distance.setFromCityId(
                    cityRepository.findByCityName(distanceDto.getCityFrom()).orElse(null)
                            .getCityId());
            } catch (NullPointerException e){
                throw new CityNotFoundException();
            }

            try{
            distance.setToCityId(
                    cityRepository.findByCityName(distanceDto.getCityTo()).orElse(null)
                            .getCityId());
        } catch (NullPointerException e){
            throw new CityNotFoundException();
        }
        distance.setDistance(distanceDto.getDistance());
        return distance;
    }
}
