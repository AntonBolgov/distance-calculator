package ru.bolgov.magenta.test.citydistance.model.entiny;

import javax.persistence.*;

@Entity
@Table(name = "distance")
public class Distance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "distance_id")
    private Long distanceId;

    @Column(name = "from_city_id")
    private Long fromCityId;

    @Column(name = "to_city_id")
    private Long toCityId;

    @Column(name = "distance")
    private int distance;

    public Distance(){}

    public Long getDistanceId() {
        return distanceId;
    }

    public void setDistanceId(Long distanceId) {
        this.distanceId = distanceId;
    }

    public Long getFromCityId() {
        return fromCityId;
    }

    public void setFromCityId(Long fromCityId) {
        this.fromCityId = fromCityId;
    }

    public Long getToCityId() {
        return toCityId;
    }

    public void setToCityId(Long toCityId) {
        this.toCityId = toCityId;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
