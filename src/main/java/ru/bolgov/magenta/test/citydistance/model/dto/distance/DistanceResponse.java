package ru.bolgov.magenta.test.citydistance.model.dto.distance;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "response")
public class DistanceResponse {

    private String calculationType;
    private DistanceDto distanceDto;

    public String getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    public DistanceDto getDistances() {
        return distanceDto;
    }

    public void setDistances(DistanceDto distanceDto) {
        this.distanceDto = distanceDto;
    }
}
