package ru.bolgov.magenta.test.citydistance.model.dto.city;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement
public class CitiesResponse {
    private List<CityResponse> cities;

    public List<CityResponse> getCities() {
        return cities;
    }

    public void setCities(List<CityResponse> cities) {
        this.cities = cities;
    }
}
