package ru.bolgov.magenta.test.citydistance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.bolgov.magenta.test.citydistance.model.dto.city.CitiesResponse;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;
import ru.bolgov.magenta.test.citydistance.repository.DistanceRepository;
import ru.bolgov.magenta.test.citydistance.service.CityService;

import java.io.IOException;

@RestController
public class CityController {

    private CityService cityService;

    @Autowired  //Уйти от аннотации @Autowired не удается, т.к. используется Spring Data. И репозиторий объявляется как интерфейс.
    public CityController(DistanceRepository distanceRepository, CityRepository cityRepository){
        this.cityService = new CityService(distanceRepository, cityRepository);
    }

    @PostMapping("/cities")
    public ResponseEntity<?> uploadCities(@RequestParam("file") MultipartFile file) throws IOException {
        cityService.updateCityMatrix(file);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/cities")
    public CitiesResponse getAllCities(){
        return cityService.getAllCities();
    }

}
