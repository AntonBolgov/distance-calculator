package ru.bolgov.magenta.test.citydistance.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Controller
@EnableWebMvc
public class UploadController {

    @GetMapping("/upload")
    public String uploadFile(){
        return "upload";
    }
}
