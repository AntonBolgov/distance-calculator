package ru.bolgov.magenta.test.citydistance.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.bolgov.magenta.test.citydistance.exception.CityNotFoundException;
import ru.bolgov.magenta.test.citydistance.exception.DistanceNotFoundException;
import ru.bolgov.magenta.test.citydistance.exception.QueryTypeNotFoundException;
import ru.bolgov.magenta.test.citydistance.mapper.DistanceMapper;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.CalculateResponse;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.DistanceDto;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.DistanceResponse;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.Distances;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;
import ru.bolgov.magenta.test.citydistance.model.entiny.Distance;
import ru.bolgov.magenta.test.citydistance.myenum.QueryTypeEnum;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;
import ru.bolgov.magenta.test.citydistance.repository.DistanceRepository;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DistanceService {

    private final DistanceRepository distanceRepository;

    private final CityRepository cityRepository;

    public DistanceService(CityRepository cityRepository, DistanceRepository distanceRepository) {
        this.cityRepository = cityRepository;
        this.distanceRepository = distanceRepository;
    }

    public void updateDistanceMatrix(MultipartFile file) throws IOException {
        XmlMapper mapper = new XmlMapper();
        Charset utfCharset = Charset.forName("UTF-8");
        String input = new String(file.getBytes(), utfCharset);
        Distances distances = mapper.readValue(input, Distances.class);
        final DistanceMapper distanceMapper = new DistanceMapper(cityRepository);

        Long count = distances.getDistanceList().stream()
                .map(x -> {
                    Distance distance = distanceMapper.dtoToEntity(x);
                    try {
                        this.getDistance(distance.getFromCityId(), distance.getToCityId());
                    }catch (DistanceNotFoundException e){
                        distanceRepository.save(distance);
                    }
                    return distance;
                })
                .count();
    }

    public CalculateResponse parseQuery(String type, String cityFrom, String cityTo){
        CalculateResponse calculateResponse = new CalculateResponse();

        List<City> citiesFrom = Stream.of(cityFrom.split(","))
                    .map(x -> cityRepository.findByCityName(x).orElse(null))
                    .collect(Collectors.toList());
            if(citiesFrom.contains(null)){
                throw new CityNotFoundException();
            }
            List<City> citiesTo = Stream.of(cityTo.split(","))
                    .map(x -> cityRepository.findByCityName(x).orElse(null))
                    .collect(Collectors.toList());
            if(citiesTo.contains(null)){
                throw new CityNotFoundException();
            }

            List<DistanceResponse> responses = new ArrayList<>();
        if(type.equalsIgnoreCase(QueryTypeEnum.CROWFLIGHT.toString()) ||
                type.equalsIgnoreCase(QueryTypeEnum.ALL.toString())){
            for (City from : citiesFrom){
                for (City to : citiesTo){
                    int distance = calculateCrowFlight(from, to);
                    DistanceResponse distanceResponse = new DistanceResponse();
                    distanceResponse.setCalculationType(QueryTypeEnum.CROWFLIGHT.toString());
                    DistanceDto distanceDto = new DistanceDto();
                    distanceDto.setCityFrom(from.getCityName());
                    distanceDto.setCityTo(to.getCityName());
                    distanceDto.setDistance(distance);
                    distanceResponse.setDistances(distanceDto);
                    responses.add(distanceResponse);
                }
            }
        }
        if(type.equalsIgnoreCase(QueryTypeEnum.MATRIX.toString()) ||
                type.equalsIgnoreCase(QueryTypeEnum.ALL.toString())) {
            for (City from : citiesFrom) {
                for (City to : citiesTo) {
                    Distance distance = getDistance(from.getCityId(), to.getCityId());
                    DistanceResponse distanceResponse = new DistanceResponse();
                    distanceResponse.setCalculationType(QueryTypeEnum.MATRIX.toString());
                    DistanceDto distanceDto = new DistanceDto();
                    distanceDto.setCityFrom(from.getCityName());
                    distanceDto.setCityTo(to.getCityName());
                    distanceDto.setDistance(distance.getDistance());
                    distanceResponse.setDistances(distanceDto);
                    responses.add(distanceResponse);
                }
            }
        }
        if(!(type.equalsIgnoreCase(QueryTypeEnum.MATRIX.toString()) ||
                type.equalsIgnoreCase(QueryTypeEnum.CROWFLIGHT.toString()) ||
                type.equalsIgnoreCase(QueryTypeEnum.ALL.toString()))){
            throw new QueryTypeNotFoundException();
        }

        calculateResponse.setDistanceResponses(responses);
        return calculateResponse;
    }

    private Distance getDistance(Long from, Long to) {
        Distance distance = distanceRepository.getByCities(from, to)
                .orElse(distanceRepository.getByCities(to, from).orElse(null));
        if(Objects.isNull(distance)){
            throw new DistanceNotFoundException();
        }
        return distance;
    }

    private int calculateCrowFlight(City from, City to){
        int earthRadius = 6371; //kilometers
        double dLat = Math.toRadians(to.getLatitude() - from.getLatitude());
        double dLng = Math.toRadians(to.getLongitude() - from.getLongitude());
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(from.getLatitude()))
                * Math.cos(Math.toRadians(to.getLatitude()));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        int distance = (int) (earthRadius * c);

        return distance;
    }
}
