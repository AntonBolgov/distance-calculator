package ru.bolgov.magenta.test.citydistance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.bolgov.magenta.test.citydistance.model.dto.distance.CalculateResponse;
import ru.bolgov.magenta.test.citydistance.repository.CityRepository;
import ru.bolgov.magenta.test.citydistance.repository.DistanceRepository;
import ru.bolgov.magenta.test.citydistance.service.DistanceService;

import java.io.IOException;
import java.util.Map;

@RestController
public class DistanceController {

    private DistanceService distanceService;

    @Autowired  //Уйти от аннотации @Autowired не удается, т.к. используется Spring Data. И репозиторий объявляется как интерфейс.
    public DistanceController(DistanceRepository distanceRepository, CityRepository cityRepository){
        this.distanceService = new DistanceService(cityRepository, distanceRepository);
    }

    @PostMapping(value = "/distances")
    public ResponseEntity<?> uploadDistances(@RequestParam("file") MultipartFile file) throws IOException {
        distanceService.updateDistanceMatrix(file);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/calculate")
    public CalculateResponse calculateDistance(@RequestParam Map<String, String> headers){

        return distanceService.parseQuery(headers.get("type"),
                headers.get("fromCity"),
                headers.get("toCity"));
    }
}
