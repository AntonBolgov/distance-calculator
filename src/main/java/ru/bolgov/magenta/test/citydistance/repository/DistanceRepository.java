package ru.bolgov.magenta.test.citydistance.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.bolgov.magenta.test.citydistance.model.entiny.Distance;

import java.util.Optional;

@Repository
public interface DistanceRepository extends CrudRepository<Distance, Long> {

    @Query("from Distance d where d.fromCityId = ?1 and d.toCityId = ?2")
    Optional<Distance> getByCities(Long from, Long to);
}
