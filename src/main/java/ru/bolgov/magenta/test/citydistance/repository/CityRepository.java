package ru.bolgov.magenta.test.citydistance.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.bolgov.magenta.test.citydistance.model.entiny.City;

import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {

    @Query("from City c where c.cityName = ?1 ")
    Optional<City> findByCityName(String name);

    List<City> findAll();
}
