package ru.bolgov.magenta.test.citydistance.model.dto.distance;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "Distance")
public class Distances {
    private List<DistanceDto> distanceList;

    public List<DistanceDto> getDistanceList() {
        return distanceList;
    }

    public void setDistanceList(List<DistanceDto> distanceList) {
        this.distanceList = distanceList;
    }
}
