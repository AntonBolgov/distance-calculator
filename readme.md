city-distance
Приложение для вычисления расстояний между городами.
Приложение разработано на Spring Boot. Сборка осуществляется с помощью maven.
Приложение собирается в исполняемый jar архив.

В приложении используется Spring data jpa и база данных MySql
Настройки к базе данных находятся в папке src/main/resources/application.properties.
По умолчанию url базы данных jdbc:mysql://app_db:3306/distance-calculator
логин anton
пароль anton
Для корректной работы приложения необходимо создать в MySql базу данных с названием distance-calculator.
Таблицы создаются при помощи liquibase.

Приложение можно запустить при помощи docker

Для сборки приложения в папке с проектом необходимо выполнить:
mvn clean install

Для сборки docker image выполнить:
docker-compose build

Для запуска приложения выполнить:
docker-compose up

После запуска приложения следует загрузить список городов с указанием широты и долготы, а также список расстояний между разными городами.
Для загрузки файлов со списком городов и расстояний предусмотрена web форма по url localhost:8080/upload
Пример xml файлов для загрузки городов и расстояний можно найти в папке /docs

Приложение позволяет получить список всех городов из базы данных по http запросу с методом GET на адрес localhost:8080/cities

Приложение позволяет получить список расстояний между заданными городами.
Для этого направляется http GET запрос по адресу localhost:8080/calculate
В заголовках запроса указываются параметры: 
-тип расчета "type", бывает трех видов:  
CROWFLIGHT - вычисление расстояния между городами по широте и долготе
MATRIX - вычисление расстояния между городами, взятое из базы 
ALL - выводятся оба варианта расстояний между городами
- список городов отправления "fromCity"
- список городов прибытия "toCity"
Пример запроса : http://localhost:8080/calculate?type=ALL&fromCity=Samara,Moscow&toCity=Omsk
